import os
import subprocess

# -------------------------------------------------------------------------------
# REQUIRED EXECUTABLES
# -------------------------------------------------------------------------------
from os.path import join

AWS_CLI_PATH = "/usr/local/bin/aws"
FFMPEG_PATH = "/home/alex/bin/ffmpeg"

# -------------------------------------------------------------------------------
# AWS CONFIGURABLE SETTINGS
# -------------------------------------------------------------------------------

BUCKET = "tefl-lms"
SOURCE = "module-sources"
USER_ID = "12"
AWS_PROFILE = "teflDev"

# -------------------------------------------------------------------------------
# AWS CLI CONFIGURABLE SETTINGS
# -------------------------------------------------------------------------------

S3_PATH = "s3://" + BUCKET + "/" + SOURCE + "/" + USER_ID + "/videos"
S3_BACKUP_PATH = S3_PATH + "/backup"

# -------------------------------------------------------------------------------
# LOCAL FOLDERS CONFIGURABLE SETTINGS
# -------------------------------------------------------------------------------

LOCAL_DOWNLOAD_PATH = os.getcwd() + "/download"
LOCAL_UPLOAD_PATH = os.getcwd() + "/upload"
LOCAL_DATABASE_PATH = os.getcwd() + "/database"

# -------------------------------------------------------------------------------
# FFMPEG CONFIGURABLE SETTINGS
# -------------------------------------------------------------------------------

VIDEO_CODEC_H264 = "libx264"
PRESET = "veryslow"
TUNE = "film"
CRF_VALUE = "28"

AUDIO_CODEC_MP3 = "libmp3lame"
AUDIO_CODEC_ACC = "libfdk_aac"
AUDIO_BR = "64K"
TEMP_FILE_PREFIX = "converting-"

# -------------------------------------------------------------------------------
# APP CONFIGURABLE SETTINGS
# -------------------------------------------------------------------------------

APP_CREATE_DATABASE_SCRIP = True
APP_ENCODE_AUDIO = False
APP_DATABASE_ID = 900
APP_USE_LOCAL_FILES = True


# -------------------------------------------------------------------------------
# APP
# -------------------------------------------------------------------------------

def main():
    print("AWS encoder started for: " + S3_PATH)
    install()


def install():
    def create_downloads_resources():
        if not APP_USE_LOCAL_FILES:
            subprocess.call(["rm", "-rf", LOCAL_DOWNLOAD_PATH])
            subprocess.call(["mkdir", LOCAL_DOWNLOAD_PATH])

    def create_uploads_resources():
        subprocess.call(["rm", "-rf", LOCAL_UPLOAD_PATH])
        subprocess.call(["mkdir", LOCAL_UPLOAD_PATH])

    def create_database_resources():
        subprocess.call(["rm", "-rf", LOCAL_DATABASE_PATH])
        subprocess.call(["mkdir", LOCAL_DATABASE_PATH])

        with open(LOCAL_DATABASE_PATH + "/db_delete.sql", "a") as f:
            f.write("--run to delete any exiting records\n")
        f.close()

        with open(LOCAL_DATABASE_PATH + "/db_insert.sql", "a") as f:
            f.write("--insert statement for files that have been encoded\n")
            f.write("INSERT INTO extoriolms_classes_models_coursefile VALUES\n")
        f.close()

    create_downloads_resources()
    create_uploads_resources()
    create_database_resources()

    if APP_USE_LOCAL_FILES:
        process(APP_DATABASE_ID)
    else:
        download()


def download():
    print("downloading from aws")

    subprocess.call([
        AWS_CLI_PATH,
        "s3", "sync",
        "--exclude", "*",
        "--include", "*.mp4",
        "--exclude", "backup/*",
        "--exclude", "oldbackup/*",
        S3_PATH, LOCAL_DOWNLOAD_PATH,
        "--profile", AWS_PROFILE,
        # "--dryrun"
    ])

    print("download complete")
    backup()


def backup():
    print("backing up")

    subprocess.call([
        AWS_CLI_PATH,
        "s3",
        "mv", "--recursive",
        "--exclude", "*",
        "--include", "*.mp4",
        "--exclude", "backup/*",
        "--exclude", "oldbackup/*",
        S3_PATH,
        S3_BACKUP_PATH,
        "--profile",
        AWS_PROFILE,
        # "--dryrun"
    ])

    print("backup complete")
    print("original files moved to: " + S3_BACKUP_PATH)
    process(APP_DATABASE_ID)


def process(app_db_id):
    file_list = filter(lambda f: f.split('.')[-1] == 'mp4', os.listdir(LOCAL_DOWNLOAD_PATH))
    file_list = sorted(file_list)

    os.chdir(LOCAL_DOWNLOAD_PATH)

    for file in file_list:
        if APP_ENCODE_AUDIO:
            encode_audio(file)
        else:
            encode_video(file, app_db_id)

        app_db_id += 1

    os.chdir("..")

    upload()


def encode_video(file, app_db_id):
    print("encoding: " + file)
    subprocess.call([
        FFMPEG_PATH,
        "-i", file,
        "-c:a", AUDIO_CODEC_ACC,
        "-b:a", AUDIO_BR,
        "-c:v", VIDEO_CODEC_H264,
        "-tune", TUNE,
        "-crf", CRF_VALUE,
        "-stats",
        "-v", "quiet",
        "-threads", "8",
        TEMP_FILE_PREFIX + file
    ])

    move_to_upload(file)

    if APP_CREATE_DATABASE_SCRIP:
        create_records(file, app_db_id)


def encode_audio(file):
    print("encoding: " + file)
    subprocess.call([
        FFMPEG_PATH,
        "-i", file,
        "-c:v", "copy",
        "-c:a", AUDIO_CODEC_ACC,
        "-b:a", AUDIO_BR,
        "-stats",
        TEMP_FILE_PREFIX + file
    ])
    print("done")
    move_to_upload(file)


def move_to_upload(file):
    subprocess.call([
        'mv',
        LOCAL_DOWNLOAD_PATH + "/" + TEMP_FILE_PREFIX + file,
        LOCAL_UPLOAD_PATH + "/" + file
    ])
    print("moved to: " + LOCAL_UPLOAD_PATH)


def upload():
    subprocess.call([
        AWS_CLI_PATH,
        "s3",
        "sync",
        "--exclude", "*",
        "--include", "*.mp4",
        LOCAL_UPLOAD_PATH,
        S3_PATH,
        "--profile",
        AWS_PROFILE,
        # "--dryrun"
    ])
    finish_records()


def create_records(file, app_db_id):
    with open(LOCAL_DATABASE_PATH + "/db_delete.sql", "a") as f:
        statement = "\nDELETE FROM extoriolms_classes_models_coursefile " \
                    "WHERE \"userId\" = '" + USER_ID + "' AND \"fileName\" =" + "'" + file + "';"
        f.write(statement)
    f.close()

    with open(LOCAL_DATABASE_PATH + "/db_insert.sql", "a") as f:
        statement = "\n('" + str(app_db_id) + "', '" \
                    + USER_ID + \
                    "', '" \
                    + SOURCE + \
                    "', 'videos', '" \
                    + file + \
                    "', '" \
                    + BUCKET + \
                    "', '" \
                    + SOURCE + \
                    "/" \
                    + USER_ID + \
                    "/videos/" \
                    + file + \
                    "', 'video/mp4'),"
        f.write(statement)
    f.close()


def finish_records():
    with open(LOCAL_DATABASE_PATH + "/db_insert.sql", 'rb+') as f:
        f.seek(-1, os.SEEK_END)
        f.truncate()
        f.close()

    with open(LOCAL_DATABASE_PATH + "/db_insert.sql", "a") as f:
        f.write(';')
    f.close()

    complete()


def complete():
    print("complete")


if __name__ == "__main__":
    main()
